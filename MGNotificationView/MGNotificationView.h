//
//  MGNotificationView.h
//  MGNotificationView
//
//  Created by Maciek Gierszewski on 10.06.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

static CGFloat const MGNotificationViewHeight = 64.0f;
static NSTimeInterval const MGNotificationViewShowHideAnimationDuration = 0.25;
static NSTimeInterval const MGNotificationViewDefaultHideDelay = 4.0;

@interface MGNotificationView : UIView
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * message;
@property (nonatomic, strong) UIView * accessory;

@property (nonatomic, assign) UIControlContentVerticalAlignment accessoryVerticalAlignment	UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) UIEdgeInsets contentPadding									UI_APPEARANCE_SELECTOR;

@property (nonatomic, assign) BOOL autoresize												UI_APPEARANCE_SELECTOR;

@property (nonatomic, strong) UIFont * titleFont											UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIFont * messageFont											UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor * titleColor											UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor * messageColor										UI_APPEARANCE_SELECTOR;

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message accessory:(UIView *)accessory tapHandlerBlock:(void (^)(void))block;

+ (instancetype)showNotificationViewWithTitle:(NSString *)title message:(NSString *)message accessory:(UIView *)accessory hideAfterDelay:(NSTimeInterval)delay tapHandlerBlock:(void (^)(void))block;
+ (instancetype)showNotificationViewWithTitle:(NSString *)title message:(NSString *)message accessory:(UIView *)accessory tapHandlerBlock:(void (^)(void))block;

- (void)showAnimated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;
- (void)hideAfterDelay:(NSTimeInterval)delay animated:(BOOL)animated;

@end
