//
//  MGNotificationViewContainerWindow.h
//  MGNotificationView
//
//  Created by Maciek Gierszewski on 10.06.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGNotificationViewController.h"

@interface MGNotificationViewContainerWindow : UIWindow
@property (nonatomic, strong) MGNotificationViewController *viewController;

+ (instancetype)sharedInstance;
@end
