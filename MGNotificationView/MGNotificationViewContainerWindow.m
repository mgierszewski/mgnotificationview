//
//  MGNotificationViewContainerWindow.m
//  MGNotificationView
//
//  Created by Maciek Gierszewski on 10.06.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import "MGNotificationViewContainerWindow.h"

@implementation MGNotificationViewContainerWindow

+ (instancetype)sharedInstance
{
    static MGNotificationViewContainerWindow *MGNotificationViewContainerWindowInstance;
    static dispatch_once_t MGNotificationViewContainerWindowOnceToken;
    dispatch_once(&MGNotificationViewContainerWindowOnceToken, ^{
        MGNotificationViewContainerWindowInstance = [MGNotificationViewContainerWindow new];
    });
    
    return MGNotificationViewContainerWindowInstance;
}

- (instancetype)init
{
    UIScreen *mainScreen = [UIScreen mainScreen];
    
    self = [super initWithFrame:mainScreen.bounds];
    if (self)
    {
        self.windowLevel = UIWindowLevelStatusBar + 1;
        self.backgroundColor = [UIColor clearColor];
        
        self.viewController = [MGNotificationViewController new];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.viewController.view.frame = self.bounds;
}

- (void)makeKeyAndVisible
{
	CGRect keyWindowBounds = [[[UIApplication sharedApplication] keyWindow] bounds];

    self.rootViewController = self.viewController;
	self.frame = (CGRect){CGPointZero, keyWindowBounds.size};
	
    [super makeKeyAndVisible];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    // pass through touches
    UIView *hitTestResult = [super hitTest:point withEvent:event];
    while (hitTestResult && ![hitTestResult isKindOfClass:[MGNotificationView class]])
    {
        hitTestResult = hitTestResult.superview;
    }
    
    return hitTestResult;
}

@end
