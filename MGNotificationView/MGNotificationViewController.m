//
//  MGNotificationViewController.m
//  MGNotificationView
//
//  Created by Maciek Gierszewski on 10.06.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MGNotificationViewController.h"
#import "MGNotificationViewContainerWindow.h"
#import "UIImage+MGNotificationView.h"

@interface MGNotificationViewController ()
@property (atomic) BOOL renderingBackground;

- (UIImage *)blurredBackgroundImageInRect:(CGRect)rect;

- (void)startRenderingBackground;
- (void)stopRenderingBackground;
- (void)backgroundRenderingRoutine;
@end

@implementation MGNotificationViewController

- (instancetype)init
{
	self = [super init];
	if (self)
	{
		_renderingBackground = NO;
	}
	return self;
}

#pragma mark - View

- (void)loadView
{
	MGNotificationViewContainerWindow *window = [MGNotificationViewContainerWindow sharedInstance];
	
	UIView *view = [[UIView alloc] initWithFrame:window.bounds];
	view.backgroundColor = [UIColor clearColor];
	view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	self.view = view;
}

#pragma mark - Status Bar Style

- (UIStatusBarStyle)preferredStatusBarStyle
{
	UIWindow *mainWindow = [[[UIApplication sharedApplication] windows] firstObject];
	UIViewController *rootViewController = mainWindow.rootViewController;
	
	while (rootViewController.presentedViewController)
	{
		rootViewController = rootViewController.presentedViewController;
	}
	
	while ([rootViewController isKindOfClass:[UINavigationController class]])
	{
		rootViewController = [(UINavigationController *)rootViewController topViewController];
	}
	
	if (rootViewController)
	{
		return [rootViewController preferredStatusBarStyle];
	}
	
	return UIStatusBarStyleDefault;
}

#pragma mark - Add / Remove

- (void)addNotificationView:(MGNotificationView *)notificationView animated:(BOOL)animated
{
	if ([self.view.subviews count] == 0)
	{
		[self startRenderingBackground];
		
		MGNotificationViewContainerWindow *window = [MGNotificationViewContainerWindow sharedInstance];
		[window makeKeyAndVisible];
	}
	else
	{
		// delay if there is already notification visible
		dispatch_async(dispatch_get_main_queue(), ^{
			[self addNotificationView:notificationView animated:animated];
		});
		return;
	}
	
	CGFloat notificationHeight = [notificationView sizeThatFits:self.view.frame.size].height;
 
	UIImageView *backgroundImageView = [notificationView valueForKey:@"backgroundImageView"];
	backgroundImageView.frame = CGRectMake(0.0f, (CGRectGetMinY(self.view.frame) + notificationHeight), CGRectGetWidth(self.view.frame), notificationHeight);
	backgroundImageView.image = [self blurredBackgroundImageInRect:CGRectMake(0.0f, CGRectGetMinY(self.view.frame), CGRectGetWidth(self.view.frame), notificationHeight)];
	
	notificationView.frame = CGRectMake(0.0f, -(CGRectGetMinY(self.view.frame) + notificationHeight), CGRectGetWidth(self.view.frame), notificationHeight);
	notificationView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
	[self.view addSubview:notificationView];
	
	// animation
	void (^animationBlock)(void) = ^{
		notificationView.frame = CGRectMake(0.0f, CGRectGetMinY(self.view.frame), CGRectGetWidth(self.view.frame), notificationHeight);
		backgroundImageView.frame = notificationView.bounds;
	};
	
	void (^completionBlock)(BOOL) = ^(BOOL flag){
	};
	
	// execution
	if (animated)
	{
		[UIView animateWithDuration:MGNotificationViewShowHideAnimationDuration delay:0.0 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState animations:animationBlock completion:completionBlock];
	}
	else
	{
		animationBlock();
		completionBlock(YES);
	}
}

- (void)removeNotificationView:(MGNotificationView *)notificationView animated:(BOOL)animated
{
	// check if it's the last notification view
	// and if so, update status bar appearance
	if ([self.view.subviews count] == 1)
	{
		[self setNeedsStatusBarAppearanceUpdate];
	}
	
	UIImageView *backgroundImageView = [notificationView valueForKey:@"backgroundImageView"];
	
	// animation
	void (^animationBlock)(void) = ^{
		notificationView.frame = CGRectMake(0.0f, -(CGRectGetMinY(self.view.frame) + CGRectGetHeight(notificationView.frame)), CGRectGetWidth(self.view.frame), CGRectGetHeight(notificationView.frame));
		backgroundImageView.frame = CGRectMake(0.0f, (CGRectGetMinY(self.view.frame) + CGRectGetHeight(notificationView.frame)), CGRectGetWidth(self.view.frame), CGRectGetHeight(notificationView.frame));
	};
	
	void (^completionBlock)(BOOL) = ^(BOOL flag){
		[notificationView removeFromSuperview];
		if ([self.view.subviews count] == 0)
		{
			[self stopRenderingBackground];
			
			MGNotificationViewContainerWindow *window = [MGNotificationViewContainerWindow sharedInstance];
			[window resignKeyWindow];
			[window setHidden:YES];
		}
	};
	
	CGFloat notificationHeight = [notificationView sizeThatFits:self.view.frame.size].height;
	
	// execution
	if (animated)
	{
		NSTimeInterval duration = MGNotificationViewShowHideAnimationDuration * MIN(1.0, CGRectGetHeight(notificationView.frame)/notificationHeight);
		[UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState animations:animationBlock completion:completionBlock];
	}
	else
	{
		animationBlock();
		completionBlock(YES);
	}
}

#pragma mark - Background rendering

- (void)startRenderingBackground
{
	self.renderingBackground = YES;
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		[self backgroundRenderingRoutine];
	});
}

- (void)stopRenderingBackground
{
	self.renderingBackground = NO;
}

- (void)backgroundRenderingRoutine
{
	while (self.renderingBackground)
	{
		__block CGFloat maxNotificationViewHeight = 0.0f;
		dispatch_sync(dispatch_get_main_queue(), ^{
			maxNotificationViewHeight = [[self.view.subviews valueForKeyPath:@"@max.layer.presentationLayer.frame.size.height"] floatValue];
		});
		
		CGRect frame = CGRectMake(CGRectGetMinX(self.view.frame), CGRectGetMinY(self.view.frame), CGRectGetWidth(self.view.frame), maxNotificationViewHeight);
		UIImage *image = [self blurredBackgroundImageInRect:frame];
		
		for (MGNotificationView *notificationView in self.view.subviews)
		{
			dispatch_sync(dispatch_get_main_queue(), ^{
				UIImageView *backgroundView = (UIImageView *)[notificationView valueForKey:@"backgroundImageView"];
				backgroundView.image = image;
			});
		}
	}
}

- (UIImage *)blurredBackgroundImageInRect:(CGRect)rect
{
	@autoreleasepool {
		
		
		UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1.0f);
		CGContextRef context = UIGraphicsGetCurrentContext();
		
		if (context == 0)
		{
			UIGraphicsEndImageContext();
			return nil;
		}
		
		CGContextTranslateCTM(context, -CGRectGetMinX(rect), -CGRectGetMinY(rect));
		CGContextClipToRect(context, rect);
		
		CGAffineTransform windowTransform = CGAffineTransformIdentity;
		if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0f)
		{
			CGFloat rotationAngle = 0.0f;
			
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
			switch (self.interfaceOrientation)
			{
				case UIInterfaceOrientationPortraitUpsideDown:
					rotationAngle = M_PI;
					break;
					
				case UIInterfaceOrientationLandscapeLeft:
					rotationAngle = M_PI_2;
					break;
					
				case UIInterfaceOrientationLandscapeRight:
					rotationAngle = -M_PI_2;
					break;
					
				case UIInterfaceOrientationPortrait:
				default:
					rotationAngle = 0.0f;
			}
#pragma clang diagnostic pop
			
			UIScreen *mainScreen = [UIScreen mainScreen];
			CGAffineTransform rotation = CGAffineTransformMakeRotation(rotationAngle);
			
			CGSize screenSize = mainScreen.bounds.size;
			CGSize transformedScreenSize = CGRectStandardize(CGRectApplyAffineTransform(mainScreen.bounds, rotation)).size;
			
			// rotate the screen
			CGContextTranslateCTM(context, transformedScreenSize.width/2.0f, transformedScreenSize.height/2.0f);
			CGContextConcatCTM(context, rotation);
			CGContextTranslateCTM(context, -screenSize.width/2.0f, -screenSize.height/2.0f);
			
			// iOS 7 needed
			windowTransform = rotation;
		}
		
		// render windows below the alert
		NSArray *windows = [[UIApplication sharedApplication] windows];
		for (UIWindow *window in windows)
		{
			if (!window.isHidden && window.windowLevel < self.view.window.windowLevel)
			{
				CGRect windowBounds = window.bounds;
				CGRect transformedWindowBounds = CGRectStandardize(CGRectApplyAffineTransform(windowBounds, windowTransform));
				
				CGAffineTransform transform = CGAffineTransformIdentity;
				transform = CGAffineTransformTranslate(transform,
													   transformedWindowBounds.size.width * window.layer.anchorPoint.x,
													   transformedWindowBounds.size.height * window.layer.anchorPoint.y);
				transform = CGAffineTransformConcat(transform, window.transform);
				transform = CGAffineTransformTranslate(transform,
													   -windowBounds.size.width * window.layer.anchorPoint.x,
													   -windowBounds.size.height * window.layer.anchorPoint.y);
				
				CGContextSaveGState(context);
				CGContextConcatCTM(context, transform);
				
				// Render the layer hierarchy to the current context
				if ([window respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)])
				{
					@try {
						[window drawViewHierarchyInRect:window.bounds afterScreenUpdates:NO];
					}
					@catch (NSException *exception) {
						NSLog(@"%@", exception);
					}
				}
				else
				{
					@try {
						[window.layer renderInContext:context];
					}
					@catch (NSException *exception) {
						NSLog(@"%@", exception);
					}
				}
				CGContextRestoreGState(context);
			}
		}
		
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		// image blur
		UIImage *returnImage = [image blurImageWithBoxSize:11];
		return returnImage;
	}
}

@end
