//
//  MGNotificationViewController.h
//  MGNotificationView
//
//  Created by Maciek Gierszewski on 10.06.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGNotificationView.h"

@interface MGNotificationViewController : UIViewController
- (void)addNotificationView:(MGNotificationView *)notificationView animated:(BOOL)animated;
- (void)removeNotificationView:(MGNotificationView *)notificationView animated:(BOOL)animated;
@end
