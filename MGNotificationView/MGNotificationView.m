//
//  MGNotificationView.m
//  MGNotificationView
//
//  Created by Maciek Gierszewski on 10.06.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import "MGNotificationView.h"
#import "MGNotificationViewContainerWindow.h"
#import "MGNotificationViewController.h"

static CGSize const DefaultAccessorySize = {20.0f, 20.0f};

@interface MGNotificationView ()

// content
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UIView *accessoryView;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIVisualEffectView *backgroundView;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL shouldHide;

@property (nonatomic, assign) BOOL delayedHideInterval;
@property (nonatomic, assign) BOOL delayedHideAnimation;

// layout
- (void)updateFramesForTitleLabel:(CGRect *)titleLabelFramePtr messageLabel:(CGRect *)messageLabelFramePtr accessoryView:(CGRect *)accessoryViewFramePtr fitSize:(CGSize *)containerSizePtr;

// handler
typedef void (^TapHandlerBlockType)(void);
@property (nonatomic, strong) TapHandlerBlockType tapHandlerBlock;

- (void)tapGestureRecognized:(UITapGestureRecognizer *)tapGestureRecognizer;
- (void)panGestureRecognized:(UIPanGestureRecognizer *)panGestureRecognizer;
- (void)timerHandler:(NSTimer *)timer;
@end

@implementation MGNotificationView

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message accessory:(UIView *)accessory tapHandlerBlock:(void (^)(void))block
{
    self = [super init];
    if (self)
    {
        super.backgroundColor = [UIColor clearColor];
        super.clipsToBounds = YES;
        
        _backgroundStyle = MGNotificationViewBackgroundStyleDark;
        
        _shouldHide = NO;
        _autoresize = NO;
        _accessoryVerticalAlignment = UIControlContentVerticalAlignmentTop;
        
        _tapHandlerBlock = block;
        _contentPadding = UIEdgeInsetsMake(6.0f + 1.0f/[[UIScreen mainScreen] scale], 15.0f, 6.0f + 1.0f/[[UIScreen mainScreen] scale], 15.0f);
        
        _backgroundView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
        
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.text = title;
        
        _messageLabel = [UILabel new];
        _messageLabel.font = [UIFont systemFontOfSize:14.0f];
        _messageLabel.textColor = [UIColor whiteColor];
        _messageLabel.numberOfLines = 0;
        _messageLabel.text = message;
        
        _contentView = [UIView new];
        _contentView.backgroundColor = [UIColor clearColor];
        
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
        _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)];
        
        [_contentView addSubview:_titleLabel];
        [_contentView addSubview:_messageLabel];
        
        if (accessory)
        {
            _accessoryView = accessory;
            [_contentView addSubview:_accessoryView];
        }
        
        [self addSubview:_backgroundView];
        [self addSubview:_contentView];
        
        [self addGestureRecognizer:_tapGestureRecognizer];
        [self addGestureRecognizer:_panGestureRecognizer];
    }
    return self;
}

+ (instancetype)showNotificationViewWithTitle:(NSString *)title message:(NSString *)message accessory:(UIView *)accessory tapHandlerBlock:(void (^)(void))block
{
    return [self showNotificationViewWithTitle:title message:message accessory:accessory hideAfterDelay:MGNotificationViewDefaultHideDelay tapHandlerBlock:block];
}

+ (instancetype)showNotificationViewWithTitle:(NSString *)title message:(NSString *)message accessory:(UIView *)accessory hideAfterDelay:(NSTimeInterval)delay tapHandlerBlock:(void (^)(void))block
{
    MGNotificationView *view = [[MGNotificationView alloc] initWithTitle:title message:message accessory:accessory tapHandlerBlock:block];
    [view showAnimated:YES];
    [view hideAfterDelay:delay animated:YES];
    
    return view;
}

// show / hide
- (void)showAnimated:(BOOL)animated
{
    // make sure execution is on main thread
    if ([NSThread isMainThread] == NO)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showAnimated:animated];
        });
        return;
    }
    
    MGNotificationViewContainerWindow *window = [MGNotificationViewContainerWindow sharedInstance];
    MGNotificationViewController *controller = window.viewController;
    
    [controller addNotificationView:self animated:animated];
    
    if (self.delayedHideInterval > 0.0)
    {
        [self.timer invalidate];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:self.delayedHideInterval target:self selector:@selector(timerHandler:) userInfo:@(self.delayedHideAnimation) repeats:NO];
    }
}

- (void)hideAnimated:(BOOL)animated
{
    // make sure execution goes on main thread
    if ([NSThread isMainThread] == NO)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideAnimated:animated];
        });
        return;
    }
    
    self.delayedHideAnimation = NO;
    self.delayedHideInterval = 0.0;
    
    [self.timer invalidate];
    self.timer = nil;
    
    self.shouldHide = NO;
    if (self.panGestureRecognizer.state == UIGestureRecognizerStateChanged || self.panGestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        // if gesture recognizer is active - wait for it to end
        self.shouldHide = YES;
        return;
    }
    
    MGNotificationViewContainerWindow *window = [MGNotificationViewContainerWindow sharedInstance];
    MGNotificationViewController *controller = window.viewController;
    
    [controller removeNotificationView:self animated:animated];
}

- (void)hideAfterDelay:(NSTimeInterval)delay animated:(BOOL)animated
{
    if ([NSThread isMainThread] == NO)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideAfterDelay:delay animated:animated];
        });
        return;
    }
    
    if (self.superview == nil)
    {
        self.delayedHideInterval = delay;
        self.delayedHideAnimation = animated;
        return;
    }
    
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:delay target:self selector:@selector(timerHandler:) userInfo:@(animated) repeats:NO];
}

#pragma mark - Timer Handler

- (void)timerHandler:(NSTimer *)timer
{
    [self hideAnimated:[timer.userInfo boolValue]];
}

#pragma mark - UIGestureRecognizers

// tap
- (void)tapGestureRecognized:(UITapGestureRecognizer *)tapGestureRecognizer
{
    if (tapGestureRecognizer.state == UIGestureRecognizerStateRecognized)
    {
        if (self.tapHandlerBlock)
        {
            self.tapHandlerBlock();
        }
        [self hideAnimated:YES];
    }
}

// pan
- (void)panGestureRecognized:(UIPanGestureRecognizer *)panGestureRecognizer
{
    switch (panGestureRecognizer.state)
    {
        case UIGestureRecognizerStateBegan:
        case UIGestureRecognizerStateChanged:
        {
            CGPoint translation = [panGestureRecognizer translationInView:panGestureRecognizer.view];
            [panGestureRecognizer setTranslation:CGPointZero inView:panGestureRecognizer.view];
            
            self.frame = UIEdgeInsetsInsetRect(self.frame, UIEdgeInsetsMake(0.0f, 0.0f, -translation.y * (translation.y > 0 ? 0.5f : 1.0f), 0.0f));
            break;
        }
        default:
        {
            CGPoint velocity = [panGestureRecognizer velocityInView:panGestureRecognizer.view];
            if (velocity.y < 0 || self.shouldHide)
            {
                [self hideAnimated:YES];
            }
            else
            {
                CGSize size = [self sizeThatFits:self.frame.size];
                [UIView animateWithDuration:0.5
                                      delay:0.0
                     usingSpringWithDamping:0.8f
                      initialSpringVelocity:0.5f
                                    options:UIViewAnimationOptionLayoutSubviews
                                 animations:^{
                                     self.frame = (CGRect){self.frame.origin, size};
                                 } completion:^(BOOL finished) {
                                 }];
            }
            break;
        }
    }
}

#pragma mark - Layout

- (void)updateFramesForTitleLabel:(CGRect *)titleLabelFramePtr messageLabel:(CGRect *)messageLabelFramePtr accessoryView:(CGRect *)accessoryViewFramePtr fitSize:(CGSize *)containerSizePtr
{
    CGFloat verticalSpacing = 1.0f;
    CGFloat horizontalSpacing = 11.0f;
    
    // accessory size
    CGSize accessorySize = CGSizeZero;
    if ([self.accessoryView isKindOfClass:[UIImageView class]])
    {
        UIImageView *accessoryImageView = (UIImageView *)self.accessoryView;
        accessorySize = CGSizeMake(MIN(accessoryImageView.image.size.width, DefaultAccessorySize.width), MIN(accessoryImageView.image.size.height, DefaultAccessorySize.height));
    }
    else if (self.accessoryView)
    {
        accessorySize = DefaultAccessorySize;
    }
    
    // title / message sizes
    CGSize contentSize = CGSizeMake(containerSizePtr->width - accessorySize.width - (accessorySize.width > 0.0f ? horizontalSpacing : 0.0f) - self.contentPadding.left - self.contentPadding.right, CGFLOAT_MAX/*MGNotificationViewHeight - self.contentPadding.top - self.contentPadding.bottom*/);
    CGSize titleSize = [self.titleLabel.text boundingRectWithSize:contentSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.titleLabel.font} context:0].size;
    CGSize messageSize = [self.messageLabel.text boundingRectWithSize:contentSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.messageLabel.font} context:0].size;
    CGSize viewSize = CGSizeMake(CGRectGetWidth(self.frame), self.autoresize ? MAX(accessorySize.height, titleSize.height + messageSize.height + (titleSize.height > 0.0f && messageSize.height > 0.0f ? verticalSpacing : 0.0f)) + self.contentPadding.top + self.contentPadding.bottom : MGNotificationViewHeight);
    
    // positions
    CGPoint titlePosition = CGPointMake(self.contentPadding.left + accessorySize.width + (accessorySize.width > 0.0f ? horizontalSpacing : 0.0f), self.contentPadding.top);
    CGPoint messagePosition = CGPointMake(titlePosition.x, titlePosition.y + titleSize.height + (titleSize.height > 0.0f ? verticalSpacing : 0.0f));
    
    CGPoint accessoryPosition = CGPointMake(self.contentPadding.left, self.contentPadding.top);
    switch (self.accessoryVerticalAlignment)
    {
        case UIControlContentVerticalAlignmentTop:
            accessoryPosition.y = MAX(self.contentPadding.top + verticalSpacing, CGRectGetMidY(*titleLabelFramePtr) - accessorySize.height/2.0);
            break;
        case UIControlContentVerticalAlignmentBottom:
            accessoryPosition.y = MAX(self.contentPadding.top + verticalSpacing, viewSize.height - self.contentPadding.bottom - verticalSpacing - accessorySize.height);
            break;
        case UIControlContentVerticalAlignmentCenter:
            accessoryPosition.y = viewSize.height/2.0 - accessorySize.height/2.0f;
            break;
        default:
            break;
    }
    
    // frames
    titleLabelFramePtr->origin = titlePosition;
    titleLabelFramePtr->size = CGSizeMake(contentSize.width, titleSize.height);
    
    messageLabelFramePtr->origin = messagePosition;
    messageLabelFramePtr->size = CGSizeMake(contentSize.width, MIN(messageSize.height, viewSize.height - self.contentPadding.bottom - messagePosition.y));
    
    accessoryViewFramePtr->origin = accessoryPosition;
    accessoryViewFramePtr->size = accessorySize;
    
    containerSizePtr->width = viewSize.width;
    containerSizePtr->height = viewSize.height;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect titleLabelFrame = self.titleLabel.frame;
    CGRect messageLabelFrame = self.messageLabel.frame;
    CGRect accessoryViewFrame = self.accessoryView.frame;
    
    CGSize containerSize = self.frame.size;
    [self updateFramesForTitleLabel:&titleLabelFrame messageLabel:&messageLabelFrame accessoryView:&accessoryViewFrame fitSize:&containerSize];
    
    if (!CGSizeEqualToSize(containerSize, self.frame.size) && self.panGestureRecognizer.state != UIGestureRecognizerStateChanged && self.panGestureRecognizer.state != UIGestureRecognizerStateBegan)
    {
        [UIView animateWithDuration:0.1 animations:^{
            self.frame = (CGRect){CGPointZero, containerSize};
        }];
    }
    
    self.backgroundView.frame = self.bounds;
    self.contentView.frame = CGRectMake(0.0f, CGRectGetHeight(self.frame) - containerSize.height, containerSize.width, containerSize.height);
    self.titleLabel.frame = titleLabelFrame;
    self.messageLabel.frame = messageLabelFrame;
    self.accessoryView.frame = accessoryViewFrame;
}

- (CGSize)sizeThatFits:(CGSize)size
{
    if (self.autoresize)
    {
        CGRect titleLabelFrame = self.titleLabel.frame;
        CGRect messageLabelFrame = self.messageLabel.frame;
        CGRect accessoryViewFrame = self.accessoryView.frame;
        
        CGSize containerSize = size;
        [self updateFramesForTitleLabel:&titleLabelFrame messageLabel:&messageLabelFrame accessoryView:&accessoryViewFrame fitSize:&containerSize];
        
        return containerSize;
    }
    
    return CGSizeMake(size.width, MGNotificationViewHeight);
}

#pragma mark - Getters

- (UIColor *)titleColor
{
    return self.titleLabel.textColor;
}

- (UIColor *)messageColor
{
    return self.messageLabel.textColor;
}

- (UIFont *)titleFont
{
    return self.titleLabel.font;
}

- (UIFont *)messageFont
{
    return self.messageLabel.font;
}

- (UIColor *)backgroundColor
{
    return self.backgroundView.backgroundColor;
}

- (NSString *)title
{
    return self.titleLabel.text;
}

- (NSString *)message
{
    return self.messageLabel.text;
}

- (UIView *)accessory
{
    return self.accessoryView;
}

#pragma mark - Setters

- (void)setBackgroundStyle:(MGNotificationViewBackgroundStyle)backgroundStyle
{
    _backgroundStyle = backgroundStyle;
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:backgroundStyle == MGNotificationViewBackgroundStyleDark ? UIBlurEffectStyleDark : UIBlurEffectStyleLight];
    self.backgroundView.effect = blurEffect;
}

- (void)setTitleColor:(UIColor *)titleColor
{
    self.titleLabel.textColor = titleColor;
}

- (void)setMessageColor:(UIColor *)messageColor
{
    self.messageLabel.textColor = messageColor;
}

- (void)setTitleFont:(UIFont *)titleFont
{
    self.titleLabel.font = titleFont;
    [self setNeedsLayout];
}

- (void)setMessageFont:(UIFont *)messageFont
{
    self.messageLabel.font = messageFont;
    [self setNeedsLayout];
}

- (void)setContentPadding:(UIEdgeInsets)contentPadding
{
    _contentPadding = contentPadding;
    [self setNeedsLayout];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    self.backgroundView.backgroundColor = backgroundColor;
}

- (void)setTitle:(NSString *)title
{
    self.titleLabel.text = title;
    [self setNeedsLayout];
}

- (void)setMessage:(NSString *)message
{
    self.messageLabel.text = message;
    [self setNeedsLayout];
}

- (void)setAccessory:(UIView *)accessory
{
    if (self.accessoryView)
    {
        [self.accessoryView removeFromSuperview];
    }
    
    _accessoryView = accessory;
    if (accessory)
    {
        [self.contentView addSubview:accessory];
    }
    [self setNeedsLayout];
}

- (void)setAutoresize:(BOOL)autoresize
{
    if (autoresize == self.autoresize)
    {
        return;
    }
    _autoresize = autoresize;
    [self setNeedsLayout];
}

@end

