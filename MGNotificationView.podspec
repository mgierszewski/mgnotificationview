Pod::Spec.new do |s|
	s.name			= "MGNotificationView"
	s.version		= "0.0.1"
	s.summary		= "Custom notification view for iOS"
	s.author		= { "Maciej Gierszewski" => "m.gierszewski@futuremind.com" }
	s.platform		= :ios, '7.0'
	s.source		= { :git => "https://bitbucket.org/mgierszewski/mgnotificationview.git", :tag => "0.0.1" }
	s.frameworks	= 'Accelerate', 'QuartzCore'
	s.source_files	= 'MGNotificationView'
	s.requires_arc	= true
	s.license		= { :type => 'WTFPL' }
	s.homepage		= 'https://bitbucket.org/mgierszewski/mgnotificationview.git'
end
